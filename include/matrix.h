#include <stdio.h>
#include <stdlib.h>
#define BRACKET_OPEN "["
#define BRACKET_CLOSE "]\n"

void print_matrix(int* matrix, int rows, int columns, const char* name);
int* matrix_init(int rows, int columns, int limit);
int* matrix_add(int* m1, int* m2, int rows, int columns);
int* matrix_mul(int* m1, int* m2, int N_1, int M, int N_2);
int matrix_sum(int* m, int rows, int columns);
int get_nearest_even(int k);


#define err(msg) printf("\033[31mERROR\033[0m: %s\n", msg)
#define die(msg) err(msg); exit(1)

#define assert_v(cond, ...) if (!(cond)) { \
	printf("\033[31mFAIL\033[0m: "); \
	printf(__VA_ARGS__); \
	assert(cond); \
}

// Returns the length of an array (or array of arrays):
//  a[5][4][3]
//  get_length(a)   => 5
//  get_length(*a)  => 4
//  get_length(**a) => 3
#define get_length(array) (sizeof(array)/sizeof(*(array)))

// Returns the total number of elements an array (or array of arrays) can hold 
//  a[5][4][3]
//  get_complete_item_count(a)   => 60
#define get_complete_item_count(array) (sizeof(array)/sizeof(double))

