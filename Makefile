CC?=clang
CFLAGS?=-O2

libmatrix.a: matrix.o
	ar rcs $@ $^

matrix.o: src/*
	$(CC) $(CFLAGS) -I include $^ -c -o $@

clean:
	rm -f $(EXEC) *.o *.a *.so .libs/*
