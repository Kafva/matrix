#include "matrix.h"

void print_matrix(int* matrix, int rows, int columns, const char* name){
	printf("\033[34m=>\033[0m %s = \n", name);
	for (int i = 0; i < rows; i++){
		printf(BRACKET_OPEN);
		for (int j = 0; j < columns; j++){
			printf(" %d ", matrix[i*rows + j]);	
		}
		printf(BRACKET_CLOSE);
	}
}

int* matrix_init(int rows, int columns, int limit){
	int* matrix = malloc(rows*columns * sizeof(int));

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++){
			printf("[%i*%i + %i =  %i]\n", i, columns, j, i*columns + j);
			int val = 0;
			for (int x = 0; x < i+j; x++  ){
				if (x%2){
					val += x;
				} else {
					val += 11;
				}
			}
			matrix[i*columns + j] = val % limit;
		}
	}
	
	return matrix;
}


/// Sum of matrix
int matrix_sum(int* m, int rows, int columns){
	int acc=0;
	for (int i = 0; i < rows; i++){
		for (int j = columns-2; j >= 0; j--){
			acc+=m[i*rows +j];	
		}
	}
	return acc;
}

int get_nearest_even(int k){
	if (k%2!=0){
		return k-1;
	} else {
		return k;
	}
}

int* matrix_add(int* m1, int* m2, int rows, int columns){
	int* m3 = (int*)calloc(rows*columns, sizeof(int));

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < columns; j++) {
			m3[i*columns + j] = m1[i*columns + j] + m2[i*columns + j];
		}
	}
	
	return m3;
}

int* matrix_mul(int* m1, int* m2, int N_1, int M, int N_2){
	// [ ROWS x COLS ]
	//  m1     * m2       = m3
	// [ N_1 x M ]   [ M x N_2 ]   [ N_1 x N_2 ]
	// The first cell in m3 will be produced from the first row in m1
	// and the first column in m2
	
	int* m3 = (int*)malloc(sizeof(int)*N_1*N_2);
	int sum;
	int m1_index, m2_index;
	
	for (int m1_row = 0; m1_row < N_1; m1_row++){
		// Once per row of m1

		for (int m2_col = 0; m2_col < N_2; m2_col++){
			// Once per column of m2
			sum = 0;

			for (int k = 0; k < M; k++){
				// Once per column in m1 
				// Once per row    in m2

				// The m1_index goes through each element of each column in a row of m1
				// The m2_index goes through each element in a column of m2
				m1_index = m1_row*M + k;
				m2_index = k*N_2 + m2_col;

				// Each row in m2 has N_2 elements, to access the next element in
				// a row-major array we therefore need to skip ahead by N_2 elements to access each item
				// and use the m2_col offset to access the correct column
				
				// printf("m1[%d]=%.1f | m2[%d]=%.1f\n",m1_index,m2_index,m1[m1_index],m2[m2_index]);
				sum += m1[m1_index] * m2[m2_index];
			}
			
			// Each iteration creates a row in m3
			// m3 will have N_1 rows and N_2 columns
			m3[ m1_row*N_2 + m2_col ] = sum;
		}
	}

	return m3;
}


